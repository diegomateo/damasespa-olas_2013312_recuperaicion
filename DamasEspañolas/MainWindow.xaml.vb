﻿Class MainWindow
    Dim pieza(24) As UserControl
    Dim rectangulo As New Rectangle
    Dim rectanguloCorrecto(7, 7)
    Dim movimientoValido(7, 7)
    Dim valido As Boolean = False
    Dim piezaMover As Object
    Dim turno As Integer = 0
    Private Sub LoadWhitePieces()
        Dim alto As Integer = 75
        Dim margen As Integer = 35
        For x As Integer = 0 To 3
            Dim peonBlanco As New PeonBlanco
            Canvas.SetTop(peonBlanco, alto)
            Canvas.SetLeft(peonBlanco, margen)
            Tablero.Children.Add(peonBlanco)
            AddHandler peonBlanco.MouseLeftButtonDown, AddressOf PieceMovement_Click
            margen += 88
        Next
        alto = 119
        margen = 79
        For x As Integer = 0 To 3
            Dim peonBlanco As New PeonBlanco
            Canvas.SetTop(peonBlanco, alto)
            Canvas.SetLeft(peonBlanco, margen)
            Tablero.Children.Add(peonBlanco)
            AddHandler peonBlanco.MouseLeftButtonDown, AddressOf PieceMovement_Click
            margen += 88
        Next
        alto = 163
        margen = 35
        For x As Integer = 0 To 3
            Dim peonBlanco As New PeonBlanco
            Canvas.SetTop(peonBlanco, alto)
            Canvas.SetLeft(peonBlanco, margen)
            Tablero.Children.Add(peonBlanco)
            AddHandler peonBlanco.MouseLeftButtonDown, AddressOf PieceMovement_Click
            margen += 88
        Next
    End Sub
    Private Sub LoadBlackPieces()
        Dim alto As Integer = 295
        Dim margen As Integer = 79
        For x As Integer = 0 To 3
            Dim peonNegro As New PeonNegro
            Canvas.SetTop(peonNegro, alto)
            Canvas.SetLeft(peonNegro, margen)
            Tablero.Children.Add(peonNegro)
            AddHandler peonNegro.MouseLeftButtonDown, AddressOf PieceMovement_Click
            margen += 88
        Next
        alto = 339
        margen = 35
        For x As Integer = 0 To 3
            Dim peonNegro As New PeonNegro
            Canvas.SetTop(peonNegro, alto)
            Canvas.SetLeft(peonNegro, margen)
            Tablero.Children.Add(peonNegro)
            AddHandler peonNegro.MouseLeftButtonDown, AddressOf PieceMovement_Click
            margen += 88
        Next
        alto = 383
        margen = 79
        For x As Integer = 0 To 3
            Dim peonNegro As New PeonNegro
            Canvas.SetTop(peonNegro, alto)
            Canvas.SetLeft(peonNegro, margen)
            Tablero.Children.Add(peonNegro)
            AddHandler peonNegro.MouseLeftButtonDown, AddressOf PieceMovement_Click
            margen += 88
        Next
    End Sub
    Private Sub LoadBoard()
        Dim left As Double = 35
        Dim top As Double = 75
        For y As Integer = 0 To 7
            For x As Integer = 0 To 7
                Dim rctBoard As New Rectangle
                rctBoard.Width = 44
                rctBoard.Height = 44
                If y Mod 2 = 0 Then
                    If x Mod 2 = 0 Then
                        rctBoard.Fill = Brushes.White
                    Else
                        rctBoard.Fill = Brushes.Black
                    End If
                Else
                    If x Mod 2 = 0 Then
                        rctBoard.Fill = Brushes.Black
                    Else
                        rctBoard.Fill = Brushes.White
                    End If
                End If
                Canvas.SetTop(rctBoard, top)
                Canvas.SetLeft(rctBoard, left)
                Tablero.Children.Add(rctBoard)
                rectanguloCorrecto(x, y) = rctBoard
                AddHandler rctBoard.MouseLeftButtonDown, AddressOf TableMovement_Click
                left += 44
            Next
            top += 44
            left = 35
        Next
    End Sub
    Private Sub Window_Initialized_1(sender As Object, e As EventArgs)
        LoadBoard()
    End Sub
    Private Sub PieceMovement_Click(sender As Object, e As RoutedEventArgs)
        For x As Integer = 0 To 7
            For y As Integer = 0 To 7
                If rectanguloCorrecto(x, y).Fill.Equals(Brushes.White) Then
                    rectanguloCorrecto(x, y).Stroke = Brushes.White
                End If
            Next
        Next
        If Not TypeOf sender Is Rectangle Then
            piezaMover = sender
            FocusMovement(sender)
        End If
    End Sub
    Private Sub TableMovement_Click(sender As Object, e As RoutedEventArgs)
        rectangulo = sender
        If turno Mod 2 = 0 Then
            If TypeOf piezaMover Is PeonBlanco Then
                If rectangulo.Fill.Equals(Brushes.White) Then
                    Do
                        For x As Integer = 0 To 7
                            For y As Integer = 0 To 7
                                If rectangulo.Stroke.Equals(Brushes.Red) Then
                                    Canvas.SetTop(piezaMover, Canvas.GetTop(sender))
                                    Canvas.SetLeft(piezaMover, Canvas.GetLeft(sender))
                                    turno += 1
                                    Exit Do
                                End If
                            Next
                        Next
                    Loop While False
                Else
                    MsgBox("Debe Ser Una Posicion Blanca")
                End If
            Else
                MsgBox("Es Turno Blanco")
            End If
        Else
            If TypeOf piezaMover Is PeonNegro Then
                If rectangulo.Fill.Equals(Brushes.White) Then
                    Do
                        For x As Integer = 0 To 7
                            For y As Integer = 0 To 7
                                If rectangulo.Stroke.Equals(Brushes.Blue) Then
                                    Canvas.SetTop(piezaMover, Canvas.GetTop(sender))
                                    Canvas.SetLeft(piezaMover, Canvas.GetLeft(sender))
                                    turno += 1
                                    Exit Do
                                End If
                            Next
                        Next
                    Loop While False
                Else
                    MsgBox("Debe Ser Una Posicion Blanca")
                End If
            Else
                MsgBox("Es Turno Negro")
            End If
        End If
        '--Cambiar a Movimiento de Reina
        If TypeOf piezaMover Is PeonBlanco Then
            Dim getTop As Integer
            Dim getLeft As Integer
            Dim damaBlanca As New ReinaBlanca
            Dim damaReinaBlanca As New PeonNegro
            getTop = Canvas.GetTop(piezaMover)
            getLeft = Canvas.GetLeft(piezaMover)
            If Canvas.GetTop(piezaMover) = 383 Then
                Tablero.Children.Remove(piezaMover)
                Canvas.SetTop(damaBlanca, getTop)
                Canvas.SetLeft(damaBlanca, getLeft)
                Tablero.Children.Add(damaBlanca)
                'damaBlanca = damaReinaBlanca
            End If
        End If
        If TypeOf piezaMover Is PeonNegro Then
            Dim getTop As Integer
            Dim getLeft As Integer
            Dim damaNegra As New ReinaNegra
            Dim damaReinaNegra As New PeonBlanco
            getTop = Canvas.GetTop(piezaMover)
            getLeft = Canvas.GetLeft(piezaMover)
            If Canvas.GetTop(piezaMover) = 75 Then
                Tablero.Children.Remove(piezaMover)
                Canvas.SetTop(damaNegra, getTop)
                Canvas.SetLeft(damaNegra, getLeft)
                Tablero.Children.Add(damaNegra)
                'damaNegra = damaReinaNegra
            End If
        End If
    End Sub
    
    Private Sub FocusMovement(pieza As Object)
        Dim posicionX As Double = 0
        Dim posicionY As Double = 0
        posicionY = Canvas.GetTop(pieza)
        posicionX = Canvas.GetLeft(pieza)
        Try
            'Movimiento Piezas Blancas
            If TypeOf pieza Is PeonBlanco Then
                For x As Integer = 0 To 7
                    If Canvas.GetLeft(rectanguloCorrecto(0, x)) = posicionX And Canvas.GetTop(rectanguloCorrecto(0, x)) = posicionY Then
                        rectanguloCorrecto(1, x + 1).Stroke = Brushes.Red
                        movimientoValido(1, x + 1) = rectanguloCorrecto(1, x + 1)
                        movimientoValido(1, x + 1).Stroke = Brushes.Red
                    End If
                Next
                For x As Integer = 0 To 7
                    For y As Integer = 0 To 7
                        If Canvas.GetLeft(rectanguloCorrecto(x, y)) = posicionX And Canvas.GetTop(rectanguloCorrecto(x, y)) = posicionY Then
                            rectanguloCorrecto(x - 1, y + 1).Stroke = Brushes.Red
                            rectanguloCorrecto(x + 1, y + 1).Stroke = Brushes.Red
                            movimientoValido(x - 1, y + 1) = rectanguloCorrecto(x - 1, y + 1)
                            movimientoValido(x + 1, y + 1) = rectanguloCorrecto(x + 1, y + 1)
                            movimientoValido(x - 1, y + 1).Stroke = Brushes.Red
                            movimientoValido(x + 1, y + 1).Stroke = Brushes.Red
                        End If
                    Next
                Next
            End If
            'Movimiento Piezas Negras
            If TypeOf pieza Is PeonNegro Then
                For x As Integer = 0 To 7
                    If Canvas.GetLeft(rectanguloCorrecto(7, x)) = posicionX And Canvas.GetTop(rectanguloCorrecto(7, x)) = posicionY Then
                        rectanguloCorrecto(6, x - 1).Stroke = Brushes.Blue
                        movimientoValido(6, x - 1) = rectanguloCorrecto(6, x - 1)
                        movimientoValido(6, x - 1).Stroke = Brushes.Blue
                    End If
                Next
                For x As Integer = 0 To 7
                    For y As Integer = 0 To 7
                        If Canvas.GetLeft(rectanguloCorrecto(x, y)) = posicionX And Canvas.GetTop(rectanguloCorrecto(x, y)) = posicionY Then
                            rectanguloCorrecto(x + 1, y - 1).Stroke = Brushes.Blue
                            rectanguloCorrecto(x - 1, y - 1).Stroke = Brushes.Blue
                            movimientoValido(x + 1, y - 1) = rectanguloCorrecto(x + 1, y - 1)
                            movimientoValido(x - 1, y - 1) = rectanguloCorrecto(x - 1, y - 1)
                            movimientoValido(x + 1, y - 1).Stroke = Brushes.Blue
                            movimientoValido(x - 1, y - 1).Stroke = Brushes.Blue
                        End If
                    Next
                Next
            End If
        Catch x As Exception
        End Try
    End Sub

    Private Sub btnJuego_Click(sender As Object, e As RoutedEventArgs) Handles btnJuego.Click
        If btnJuego.Content = "Nuevo Juego" Then
            LoadBlackPieces()
            LoadWhitePieces()
            btnJuego.Content = "Reiniciar"
        ElseIf btnJuego.Content = "Reiniciar" Then
            RebootGame()
        End If
    End Sub
    Private Sub RebootGame()
        If btnJuego.Content = "Reiniciar" Then
            Tablero.Children.Clear()
            Tablero.Children.Add(rctBackgroud)
            LoadBoard()
            btnJuego.Content = "Nuevo Juego"
        End If
    End Sub
End Class